/**
 * Permet de mettre a jour les graphes en fonction de la region et du secteur selectionne.
 * 
 * @param {*} region region de france courante
 * @param {*} secteur secteur d'activite courant
 */
function updateCharts(region, secteur) {
    if (region != 'all') {
        if (secteur != 'all') {
            new Chartkick.LineChart("chart-1", supprimerValeurNulles(donneesJSON['emplois'][region][secteur]), { colors: [getColor(region), getColor(region)], points: false, suffix: " personnes", thousands: "," });
        } else {
            new Chartkick.LineChart("chart-1", supprimerValeurNulles(donneesJSON['emplois'][region][secteur]), { colors: [getColor(region), getColor(region)], points: false, suffix: " personnes", max: 6000000, thousands: "," });
        }
        new Chartkick.LineChart("chart-2", donneesJSON['pib'][region], { colors: [getColor(region), getColor(region)], points: false, suffix: " millions d'euros", max: 700 });
        new Chartkick.LineChart("chart-3", donneesJSON['pibHabitant'][region], { colors: [getColor(region), getColor(region)], points: false, suffix: " milliers d'euros", min: 10, max: 60 });
        new Chartkick.LineChart("chart-4", donneesJSON['tauxChomage'][region], { colors: [getColor(region), getColor(region)], points: false, suffix: "%", min: 5, max: 15 });
        titre.innerHTML = region;
    } else {
        if (secteur != 'all') {
            new Chartkick.LineChart("chart-1", toChartKickMultipleSeriesAvecSecteur(donneesJSON['emplois']), { legend: true, points: false, suffix: " personnes", thousands: "," });
        } else {
            new Chartkick.LineChart("chart-1", toChartKickMultipleSeriesAvecSecteur(donneesJSON['emplois']), { legend: true, points: false, max: 6000000, suffix: " personnes", thousands: "," });
        }
        new Chartkick.LineChart("chart-2", toChartKickMultipleSeries(donneesJSON['pib']), { legend: false, points: false, max: 700, suffix: " millions d'euros" });
        new Chartkick.LineChart("chart-3", toChartKickMultipleSeries(donneesJSON['pibHabitant']), { legend: false, points: false, suffix: " milliers d'euros", min: 10, max: 60 });
        new Chartkick.LineChart("chart-4", toChartKickMultipleSeries(donneesJSON['tauxChomage']), { legend: false, points: false, suffix: "%", min: 5, max: 15 });
        titre.innerHTML = "Toute La France";
    }
}
/**
 * Permet de supprimer les valeur vides 
 * 
 * 
 * @param {*} donnees un tableau de donnees comme donneesJSON['emplois'] par exemple
 */
function supprimerValeurNulles(donnees) {
    let tmpArray = [];
    for (const key in donnees) {
        if (donnees.hasOwnProperty(key)) {
            const element = donnees[key];
            if (element) {
                tmpArray[key] = element;
            }
        }
    }
    return toObject(tmpArray);
}

/**
 * Permet de convertir une donnee en objet pour etre utilisable par les graphes Chartkick
 * 
 * @param {*} donnees 
 */
function toChartKickMultipleSeries(donnees) {
    let donneesChartKick = [];
    for (const cle in donnees) {
        if (donnees.hasOwnProperty(cle)) {
            const donneesRegionales = donnees[cle];
            donneesChartKick.push({
                name: cle,
                data: donneesRegionales
            })
        }
    }
    donneesChartKick.sort((a, b) => {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    })
    return donneesChartKick;
}

/**
 * Permet de convertir une donnee en objet pour etre utilisable par les graphes Chartkick (avec les secteurs)
 * 
 * @param {*} donnees 
 */
function toChartKickMultipleSeriesAvecSecteur(donnees) {
    let donneesChartKick = [];
    for (const cle in donnees) {
        if (donnees.hasOwnProperty(cle)) {
            const donneesRegionales = donnees[cle][secteurCourant];
            donneesChartKick.push({
                name: cle,
                data: donneesRegionales
            })
        }
    }
    donneesChartKick.sort((a, b) => {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    });
    if (secteurCourant == 'all' || secteurCourant == 'Agriculture' || secteurCourant == 'Tertiaire non marchand') {
        let tmpArray = [];
        for (let index = 0; index < donneesChartKick.length; index++) {
            tmpArray = [];
            for (const key in donneesChartKick[index]["data"]) {
                if (donneesChartKick[index]["data"].hasOwnProperty(key)) {
                    const element = donneesChartKick[index]["data"][key];
                    if (element) {
                        tmpArray[key] = element;
                    }
                }
            }
            donneesChartKick[index]["data"] = toObject(tmpArray);
        }
    }
    return donneesChartKick;
}