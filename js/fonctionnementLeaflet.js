/* Fonctions du Tutorial LeafLet */

/**
 * Retourne la couleur associes a la region
 * 
 * @param {*} region
 */
function getColor(region) {
    switch (region) {
        case 'Auvergne-Rhône-Alpes':
            return '#3366cc';
        case 'Bourgogne-Franche-Comté':
            return "#dc3912"
        case 'Bretagne':
            return '#ff9900';
        case 'Centre-Val de Loire':
            return '#109618';
        case 'Corse':
            return '#990099';
        case 'Grand Est':
            return '#3b3eac';
        case 'Hauts-de-France':
            return '#0099c6';
        case 'Île-de-France':
            return '#22aa99';
        case 'Normandie':
            return '#dd4477';
        case 'Nouvelle-Aquitaine':
            return '#66aa00';
        case 'Occitanie':
            return '#b82e2e';
        case 'Pays de la Loire':
            return '#316395';
        case 'Provence-Alpes-Côte d\'Azur':
            return '#994499'
    }
}

/**
 * Permet de 'colorier' les regions sur la carte
 * 
 * @param {*} feature 
 */
function style(feature) {
    return {
        fillColor: getColor(feature.properties.nom),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

/**
 * Defini le comportement quand on passe la souris sur une region
 * 
 * @param {*} e 
 */
function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    layer.openPopup(); 

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

/**
 * Defini le comportement quand la souris ne survole plus une region
 * 
 * @param {*} e 
 */
function resetHighlight(e) {
    var layer = e.target;
    layer.closePopup();
    geojson.resetStyle(e.target);

}

/**
 * Defini le comportement quand on clique sur une region
 * 
 * @param {*} e 
 */
function click(e) {
    regionCourante = e.target.feature.properties.nom;
    updateCharts(regionCourante, secteurCourant);
}

/**
 * Applique les fonctions sur la carte
 * 
 * @param {*} feature 
 * @param {*} layer 
 */
function onEachFeature(feature, layer) {
    layer.bindPopup(feature.properties.nom, {closeButton: false, offset: L.point(0, -10)});
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: click
    });
}

/* Fin Fonctions du Tutorial LeafLet */