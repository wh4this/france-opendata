/* Variables et constantes globales */

// L'objet qui stocke les chemins relatif des fichiers (relatif au fichier 'index.html')
const fichiersJSON = {
    emplois: 'Données/Emplois/emplois-par-region-par-secteur-france2010-2018.json',
    pib: 'Données/PIB/PIB-par-region-france1990-2015.json',
    pibHabitant: 'Données/PIB par habitant/PIB-par-hab-par-region-france1990-2015.json',
    tauxChomage: 'Données/Taux de chomage/taux-chomage-par-region-france1982-2018.json',
    regions: 'Données/Regions de France/regions-france.geojson'
};

// Le tableau qui va stocker les donnees
const donneesJSON = [];

// Le tableau qui stocke les noms des regions
// Hors DOM-TOM
const nomsRegions = [
    'Auvergne-Rhône-Alpes',
    'Bourgogne-Franche-Comté',
    'Bretagne',
    'Centre-Val de Loire',
    'Corse',
    'Grand Est',
    'Hauts-de-France',
    'Île-de-France',
    'Normandie',
    'Nouvelle-Aquitaine',
    'Occitanie',
    'Pays de la Loire',
    'Provence-Alpes-Côte d\'Azur',
];

// Le tableau qui stocke les noms des secteurs detectes
let nomsSecteurs = [];

// La liste des secteurs que l'on ne veut pas utiliser
const listeNoirNomsSecteurs = [
    "Ensemble des salariés",
    "Ensemble des salariés du secteur public",
    "Ensemble des salariés du secteur privé",
    "Salariés des secteurs marchands non agricoles (hors particuliers employeurs)",
    "Activités scientifiques et techniques ; services administratifs et de soutien",
]

let regionCourante = "all";
let secteurCourant = "all";

let geojson;

let titre = document.getElementById('titre');