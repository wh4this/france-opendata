/* Fonction du chargement et de l'affichage */

/**
 * Charge les fichiers JSON et GeoJSON
 * 
 * @param {*} fichiers 
 * @param {*} callback 
 */
function charger(fichiers, callback) {
    var isChromium = window.chrome;
    var winNav = window.navigator;
    var vendorName = winNav.vendor;
    var isOpera = typeof window.opr !== "undefined";
    var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
    var isIOSChrome = winNav.userAgent.match("CriOS");
    
    if (isIOSChrome) {
       // is Google Chrome on IOS
    } else if(
      isChromium !== null &&
      typeof isChromium !== "undefined" &&
      vendorName === "Google Inc." &&
      isOpera === false &&
      isIEedge === false
    ) {
       chargerFichierJSONChrome(donneesJSON, callback);
       console.log('Chargement terminé');
    } else { 
        for (const cle in fichiers) {
            if (fichiers.hasOwnProperty(cle)) {
                const fichier = fichiers[cle];
                chargerFichierJSON(fichier, function (responseText) {
                    donneesJSON[cle] = JSON.parse(responseText);
                    if (Object.keys(donneesJSON).length === Object.keys(fichiers).length) {
                        console.log('Chargement terminé');
                        callback();
                    }
                });
            }
        }
    }
}

/**
 * Initialise la carte, les graphes, les donnees et les noms de secteur d'activite
 * 
 */
function main() {
    Chartkick.configure({language: "fr"})

    for (const cle in donneesJSON) {
        if (donneesJSON.hasOwnProperty(cle)) {
            if (cle !== 'regions' && cle !== 'name') {
                donneesJSON[cle] = transformerDonneesJSON(cle);
                associerAnneeEtDonnees(cle);
            }
        }
    }

    console.log(donneesJSON);

    // Initialisation de mapbox et leaflet
    let mapboxAccessToken = 'pk.eyJ1Ijoid2g0dGhpcyIsImEiOiJjam5pb2szZzAwbTJ0M3BwN2Z4Ymo2NGh3In0.I0x0lvpESdUe8_VKsgYlXw';
    let map = L.map('map', {
        doubleClickZoom: false,
        scrollWheelZoom: false,
    }).setView([46.364716, 2.349014], 6);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + mapboxAccessToken, {
        id: 'mapbox.light',
    }).addTo(map);

    geojson = L.geoJson(donneesJSON.regions).addTo(map);

    geojson = L.geoJson(donneesJSON.regions, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(map);

    info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this._div.innerHTML = '<button class="toute-la-france" onclick="regionCourante = \'all\'; updateCharts(regionCourante, secteurCourant);">Toute la France</button>';
        return this._div;
    };

    info.addTo(map);

    updateCharts('all', 'all');

    nomsSecteurs = nomsSecteurs.filter(nom => {
        return listeNoirNomsSecteurs.indexOf(nom) === -1;
    });

    nomsSecteurs.forEach(nom =>
        selectSecteurOptionList.add(
            new Option(nom, nom, null)
        )
    );
}

/* Fin Fonction du chargement et de l'affichage */

charger(fichiersJSON, main);

